# _*_ coding: utf-8
#Working python file to use until an automatic template reader and a template selector are implemented
import xlsxwriter
from lxml import objectify, etree

cutoff = 1


def get_date(raw_date):
    date = raw_date[6] + raw_date[7] + '/' #add day
    date += raw_date[4] + raw_date[5] + '/' #add month
    date += raw_date[0] + raw_date[1] + raw_date[2] + raw_date[3] #add year
    return date


def write_top(worksheet, bold):
    worksheet.write(0, 0, 'Numero', bold)
    worksheet.write(0, 1, 'Client', bold)
    worksheet.write(0, 2, 'Titre', bold)
    worksheet.write(0, 3, 'Date', bold)
    worksheet.write(0, 4, 'Remise', bold)
    worksheet.write(0, 5, 'Total HT', bold)
    worksheet.write(0, 6, 'TVA', bold)
    worksheet.write(0, 7, 'Total TTC', bold)


def xml_parse(file, workbook, boolean):
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': True})
    money = workbook.add_format()
    write_top(worksheet, bold)

    xml = open(file).read()
    all = objectify.fromstring(xml)

    i = 2
    for facture in all.Data:

        # id = facture.Numero
        # client = facture.ClasserSous
        # title = facture.Titre
        # date = get_date(str(int(facture.Date)))
        # cutoff = facture.TotalRemise
        # totalHT = facture.TotalNetHT
        # taxe = facture.TotalTVA
        # totalTTC = facture.TotalTTC
        # array = np.array([id, client, title, date, cutoff, totalHT, taxe, totalTTC])
        # print array

        if int(facture.TotalTTC) < 0:
            if boolean == 1:
                worksheet.write(i, 8, "AVOIR")
            else:
                continue

        worksheet.write(i, 0, facture.Numero)
        worksheet.write(i, 1, facture.ClasserSous.text)
        worksheet.write(i, 2, facture.Titre.text)
        worksheet.write(i, 3, get_date(str(int(facture.Date))))
        worksheet.write(i, 4, float(facture.TotalRemise), money)
        worksheet.write(i, 5, float(facture.TotalNetHT), money)
        worksheet.write(i, 6, float(facture.TotalTVA), money)
        worksheet.write(i, 7, float(facture.TotalTTC), money)
        i += 1
        money.set_font_color('black')


file_name = raw_input("File name: ")
cutoff_input = str(raw_input("Voulez vous conserver les avoirs? Oui(O) ou Non(N) "))
if cutoff_input == "N" or cutoff_input == "n":
    cutoff = 0
else:
    cutoff = 1
workbook = xlsxwriter.Workbook(file_name + '.xlsx')
xml_parse(file_name + '.xml', workbook, cutoff)
workbook.close()